<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Utama</title>
</head>
<body>
    @extends('layout.master')
    @section('title')
    HALAMAN UTAMA
    @endsection

    @section('content')
    <ul>
        <li>Data 1</li>
        <li>Data 2</li>
        <li>Data 3</li>
    </ul>
    @endsection

</body>
</html>