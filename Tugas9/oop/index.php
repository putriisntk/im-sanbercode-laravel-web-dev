<?php

require_once('animal.php');
$sheep = new Animal("Shaun");

echo "Name : " . $sheep->hewan . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // "no"
echo "---------------------------------";
echo "<br>";

require_once('frog.php');
$kodok = new Frog("buduk");
echo "Name : " . $kodok->hewan . "<br>";
echo "Legs : " . $kodok->legs . "<br>"; 
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; 
// echo "Jump : " . $kodok->jump . "<br>";
echo $kodok->jump() . "<br>"; 
echo "---------------------------------";
echo "<br>";


require_once('ape.php');
$sungokong = new ape("kera sakti");
echo "Name : " . $sungokong->hewan . "<br>"; 
echo "Legs : " . $sungokong->legs . "<br>"; 
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo $sungokong->things("Auooo") . "<br>"; 
// echo "Yell : " . $sungokong->yell . "<br>";
echo "<br>";


   

?>